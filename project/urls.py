from django.conf.urls import patterns, include, url
import settings
from django.conf.urls.static import static
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'lavanderia.views.home', name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^prendas/', include('lavanderia.prenda.urls')),
    url(r'^clientes/', include('lavanderia.clientes.urls')),
    url(r'^prenda/', include('lavanderia.prenda.urls')),
    url(r'^login/', include('lavanderia.login.urls')),
    url(r'^logout$', 'lavanderia.login.views.logout_auth', name='r'),

    )
#+ static(settings.STATIC_URL, document_root="settings.STATIC_ROOT")
if settings.DEBUG:
  urlpatterns += patterns('',
  (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT, 'show_indexes':True}),)
