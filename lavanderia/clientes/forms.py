# -*- coding: utf-8 -*-
from django.forms import ModelForm

from lavanderia.clientes.models import Client


class ClientForm(ModelForm):
    class Meta:
      model = Client
