from django.conf.urls import patterns,  url

urlpatterns = patterns('',
url(r'^$', 'lavanderia.clientes.views.lista', name='r'),
url(r'^registrar$', 'lavanderia.clientes.views.registrar', name='r'),
url(r'^ver/([0-9])$', 'lavanderia.clientes.views.ver', name='r'),
url(r'^buscar$', 'lavanderia.clientes.views.buscar', name='r'),
)