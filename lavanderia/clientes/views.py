from django.shortcuts import render_to_response,    redirect
from django.template import RequestContext
from lavanderia.clientes.forms import ClientForm
from lavanderia.clientes.models import Client
from django.contrib.auth.decorators import login_required


@login_required(login_url='/login')
def lista(request):
    clientes = Client.objects.all()
    data = {'clientes': clientes}
    return render_to_response('lista_clientes.html', data,
                              context_instance=RequestContext(request))

@login_required(login_url='/login')
def ver(request, id):
    cliente = Client.objects.get(id=id)
    data = {'cliente': cliente}
    return render_to_response('ver_clientes.html', data,
                              context_instance=RequestContext(request))
@login_required(login_url='/login')
def buscar(request):
    if request.method == 'POST':
        dni = request.POST.get('dni',)
        try:
            cliente = Client.objects.filter(dni=dni)[0]
            print cliente
            return redirect('/prenda/ingresar/%s' % dni)
        except:
            return redirect('/clientes/registrar')
    else:
        return redirect('/clientes/registrar')

@login_required(login_url='/login')
def registrar(request):
    if request.method == 'POST':
        dni = request.POST.get('dni',)
        form = ClientForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/prenda/ingresar/%s" % dni)
    else:
        form = ClientForm()
    data = {'form': form}
    print form.errors
    return render_to_response('registro_clientes.html', data,
                              context_instance=RequestContext(request))