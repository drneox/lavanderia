from django.db import models

class Client(models.Model):
    name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=40)
    dni = models.CharField(max_length=8)
    email = models.EmailField()
    address = models.TextField()
    cellphone = models.IntegerField(max_length=13)

    def __unicode__(self):
        return ("%s %s") % (self.name, self.last_name)
