from django.contrib import admin
from lavanderia.prenda.models import Client, Garment, TypeGarment

admin.site.register(Client)
admin.site.register(TypeGarment)

class ProductAdmin(admin.ModelAdmin):
    list_display = ('description', 'price')


admin.site.register(Garment, ProductAdmin)
#admin.site.register(Ticket)
