from django.conf.urls import patterns, url

urlpatterns = patterns('',
url(r'^$', 'lavanderia.prenda.views.ver_tipo', name='r'),
url(r'^tipo$', 'lavanderia.prenda.views.registrar_tipo', name='r'),
#url(r'^buscar$', 'lavanderia.prenda.views.buscar', name='r'),
url(r'^ingresar/(\d+)/$', 'lavanderia.prenda.views.ingresar', name='r'),
url(r'^ingresar/$', 'lavanderia.prenda.views.ingresar', name='r'),
url(r'^borrar/([0-9])$', 'lavanderia.prenda.views.borrar_tipo', name='r'),
url(r'^resultado/(\d+)$', 'lavanderia.prenda.views.resultado', name='r'),
url(r'^ver/$', 'lavanderia.prenda.views.ver', name='r'),
url(r'^status/(\d+)$', 'lavanderia.prenda.views.status_update', name='r'),
url(r'^logout$', 'lavanderia.login.views.logout_auth', name='r'),
)