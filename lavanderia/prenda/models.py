# -*- coding: utf-8 -*-
from django.db import models
from lavanderia.clientes.models import Client

class TypeGarment(models.Model):
    name = models.CharField(max_length=40, verbose_name=u'Nombre')
    price = models.DecimalField(max_digits=5, decimal_places=2, verbose_name=u'Precio por Kilo')

    def __unicode__(self):
        return self.name


class Garment(models.Model):
    CHOICES_COLOR = (('azul', 'azul'),
                    ('rojo', 'rojo'),
                    ('amarillo', 'amarillo'),
                    ('verde', 'verde'),
                    ('blanco', 'blanco'),
                    ('negro', 'negro'))
    CHOICES_STATUS = (('R', 'Recibido'),
                    ('L', 'Lavado'),
                    ('S', 'Secado'),
                    ('P', 'Planchado'),
                    ('A', 'Almacenado'),
                    ('E', 'Entregado'))
    description = models.CharField(max_length=30, verbose_name=u'Descripción')
    type = models.ForeignKey(TypeGarment, verbose_name=u'Tipo')
    weight = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='peso')
    color = models.CharField(choices=CHOICES_COLOR, max_length=20)
    check_in = models.DateField(auto_now_add=True, verbose_name=u'ingreso')
    check_out = models.DateField(verbose_name=u'Fecha de entrega')
    status = models.CharField(max_length=1, choices=CHOICES_STATUS, default=CHOICES_STATUS[0][0])
    location = models.CharField(max_length=30, verbose_name=u'Ubicación', null=True, blank=True)
    client = models.ForeignKey(Client)

    def __unicode__(self):
        return self.description

    def get_status(self):
        status = dict(self.CHOICES_STATUS)
        return status[self.status]

    def price(self):
        return self.type.price * self.weight

'''class Ticket(models.Model):
    client = models.ForeignKey(Client)
    product = models.ManyToManyField(Product)

    def __unicode__(self):
        return  str(self.id)
'''