from django.shortcuts import render, render_to_response, redirect
from lavanderia.prenda.forms import TypeGarmentForm, GarmentForm, UpdateStatusForm
from django.template import RequestContext
from lavanderia.prenda.models import TypeGarment, Garment
from lavanderia.clientes.models import Client
from django.contrib.auth.decorators import login_required


@login_required(login_url='/login')
def ver_tipo(request):
    tipos = TypeGarment.objects.all()
    data = {'tipos': tipos}
    return render_to_response('index_prenda.html', data,
                              context_instance=RequestContext(request))

@login_required(login_url='/login')
def borrar_tipo(request,tipo):
    tipo = TypeGarment.objects.get(id=tipo)
    tipo.delete()
    return redirect("/prendas")

@login_required(login_url='/login')
def registrar_tipo(request):
    if request.method == 'POST':
        form = TypeGarmentForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/prendas")
    else:
        form = TypeGarmentForm()
    data = {'form': form}
    print form.errors
    return render_to_response('registro_prenda.html', data,
                              context_instance=RequestContext(request))


@login_required(login_url='/login')
def ingresar(request, dni=None):
    if request.method == 'POST':
        form = GarmentForm(request.POST)
        if form.is_valid():
            id = request.POST.get('client',)
            client = Client.objects.get(id=id)
            form = form.save(commit=False)
            form.client = client
            form.save()
            if request.POST.get('final',):
                return redirect("/prendas/resultado/%s" % form.id)
            else:
                return redirect("/prenda/ingresar/%s" % dni)
        data = {'form': form}
    else:
        if dni is None:
            return redirect("/prendas/nueva")
        form = GarmentForm()
        client = Client.objects.get(dni=dni)
        data = {"form": form, "client": client}
    #data = {'form': form}
    print form.errors
    return render_to_response('ingreso_prenda.html', data,
                              context_instance=RequestContext(request))

@login_required(login_url='/login')
def ver(request):
    garment = Garment.objects.all()
    data = {'garments': garment}
    return render_to_response('prendas_ver.html', data,
                              context_instance=RequestContext(request))

@login_required(login_url='/login')
def status_update(request, id):
    garment = Garment.objects.get(id=id)
    if request.method == 'POST':
        form = UpdateStatusForm(request.POST)
        if form.is_valid():
            status = request.POST.get('status', )
            location = request.POST.get('location')
            garment.status = status
            garment.location = location
            garment.save()
            return redirect("/prendas/ver")
    else:
        form = UpdateStatusForm()
    data = {'form': form, 'garment': garment}
    return render_to_response('status_update.html', data,
                                  context_instance=RequestContext(request))


@login_required(login_url='/login')
def resultado(request, id=None):
    garment = Garment.objects.get(id=id)

    data = {'garment': garment}
    return render_to_response('resultado_prenda.html', data,
                              context_instance=RequestContext(request))

