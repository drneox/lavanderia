# -*- coding: utf-8 -*-
from django.forms import ModelForm

from lavanderia.prenda.models import TypeGarment, Garment


class TypeGarmentForm(ModelForm):
    class Meta:
        model = TypeGarment
                #exclude = ('user', 'mission')


class GarmentForm(ModelForm):
    class Meta:
        model = Garment
        exclude = ('client', 'status')


class UpdateStatusForm(ModelForm):
    class Meta:
        model = Garment
        exclude = ('description', 'type', 'weight', 'color', 'check_in', 'check_out','client')