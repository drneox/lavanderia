from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout


def login_auth(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect("/clientes")
            else:
                return redirect("/login")
    else:
        return render_to_response("login_auth.html",  context_instance=RequestContext(request))


def logout_auth(request):
    logout(request)
    return redirect("/login")